<!DOCTYPE html>
<html>
    <!-- LEMBRANDO DE PASSAR A CODIFICAÇÃO DA PÁGINA -->
    <meta charset="UTF-8"
          <head>
        <!-- CRIEI UMA VARIVEL QUE RECE UM VALOR, SE ESTIVER VAZIO, EXIBE O TEXTO -->
        <title>{{$titulo or 'Painel Curso'}}</title>
        <!-- ANTES DE FECHAR O HEAD EU INSIRO OS ARQUIVOS CSS-->
        <link rel="stylesheet" href="{{url('/assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{url('/assets/css/bootstrap-theme.min.css')}}">
    </head>
    <body>
        <!-- O CONTEUDO DINÂMICO VEM DENTRO DESSA FUNÇÃO PASSANDO PRA ESSE ESCOPO -->
        <div class="container">
            @yield('content')
        </div>
        <!-- ANTES DO FECHAR O BODY EU INSIRO OS ARQUIVOS JS-->
        <script src="{{url('/assets/js/jquery-2.2.4.js')}}"></script>
        <script src="{{url('/assets/js/bootstrap.min.js')}}"></script>
    </body>
</html>