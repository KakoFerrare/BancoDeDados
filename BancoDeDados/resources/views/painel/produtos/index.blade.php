{{-- AQUI EU SETEI QUE ESTA PAGINA VAI UTILIZAR O TEMPLATE QUE ESTA NO CAMINHO --}}
@extends('painel.templates.painel')

{{-- COMO EU DEFINI QUE USARIA O TEMPLATE ACIMA, NELE EU TENHO UM yield('content')
    QUE DIZ QUE VAI RECEBER DINAMICAMENTE UM CONTEUDO, QUE FOI DEFINIDO ABAIXO,
    TUDO QUE ESTIVER DENTRE DESSA SECTION, SERÁ RENDERIZADO LA--}}
@section('content')

<br>
<div class="top-header">
    <h1 class="titulo">Listagens dos Produtos</h1>
    <br>
    <a href="{{'/produto/create'}}" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Novo Produto</a>
</div>

<table class="table table-striped">
    <thead>
    <th>Id</th>
    <th>Produto</th>
    <th>Código do Produto</th>
    <th width="150px">Ações</th>
</thead>
<tbody>
    @forelse($produtos as $produto)
    <tr>
        <td>{{$produto->id}}</td>
        <td>{{$produto->nome}}</td>
        <td>{{$produto->cod}}</td>
        <td><a href="{{url("/produto/$produto->id/edit")}}"><i class="glyphicon glyphicon-pencil"></i></a>
            <a href="{{url("/produto/$produto->id")}}"><i class="glyphicon glyphicon-info-sign"></i></a></td>
    </tr>
    @empty
    <tr>
        <td colspan="3">Nenhum registro cadastrado</td>
    </tr>
    @endforelse
</tbody>
</table>

{!! $produtos->links() !!}
@endsection