{{-- AQUI EU SETEI QUE ESTA PAGINA VAI UTILIZAR O TEMPLATE QUE ESTA NO CAMINHO --}}
@extends('painel.templates.painel')

{{-- COMO EU DEFINI QUE USARIA O TEMPLATE ACIMA, NELE EU TENHO UM yield('content')
    QUE DIZ QUE VAI RECEBER DINAMICAMENTE UM CONTEUDO, QUE FOI DEFINIDO ABAIXO,
    TUDO QUE ESTIVER DENTRE DESSA SECTION, SERÁ RENDERIZADO LA--}}
@section('content')

<!-- AQUI EU PERGUNTO SE TEM $errors SETADO E SE ESTA MAIOR QUE ZERO
 SE SIM ELE PERCORRE O ARRAY DE ERROS EXIBINDO AS MENSAGENS-->
@if(isset($errors) && count($errors) > 0)
<div class="alert alert-danger">
    @foreach($errors->all() as $error)
    {{$error}}<br>
    @endforeach
</div>
@endif

<br>
<div class="top-header">
    <h1 class="titulo">Gestão Produto</h1>
    <br>
    
</div>

@if(isset($produto->id))
<form class="form" method="post" action="/produto/{{$produto->id}}">
    <!-- PRECISO CRIAR O INPUT ESPEFICANDO O TIPO DA REQUISIÇÃO -->
    <input name="_method" value="PUT" type="hidden">
@else
<form class="form" method="post" action="/produto">
@endif

    {!! csrf_field() !!}

    <div class="form-group">
        <input type="text" name="nome" placeholder="Nome do Produto" class="form-control" value="@if(isset($produto->nome)){{$produto->nome}}@else{{old('nome')}}@endif">
    </div>
    <div class="form-group">
        <input type="text" name="cod" placeholder="Código do Produto" class="form-control" value="@if(isset($produto->cod)){{$produto->cod}}@else{{old('cod')}}@endif">
    </div>
<a href="{{'/produto'}}" class="btn btn-primary"><i class="glyphicon glyphicon-backward"></i> Voltar</a>
    <input type="submit" name="enviar" value="Enviar" class="btn btn-success">
</form>



@endsection