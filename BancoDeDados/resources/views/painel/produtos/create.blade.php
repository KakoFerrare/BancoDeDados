{{-- AQUI EU SETEI QUE ESTA PAGINA VAI UTILIZAR O TEMPLATE QUE ESTA NO CAMINHO --}}
@extends('painel.templates.painel')

{{-- COMO EU DEFINI QUE USARIA O TEMPLATE ACIMA, NELE EU TENHO UM yield('content')
    QUE DIZ QUE VAI RECEBER DINAMICAMENTE UM CONTEUDO, QUE FOI DEFINIDO ABAIXO,
    TUDO QUE ESTIVER DENTRE DESSA SECTION, SERÁ RENDERIZADO LA--}}
@section('content')

<br>
<div class="top-header">
    <h1 class="titulo">Gestão de Produto</h1>
    <br>
    <a href="{{'/produto'}}" class="btn btn-primary"><i class="glyphicon glyphicon-backward"></i> Voltar</a>
</div>


<!-- AQUI EU PERGUNTO SE TEM $errors SETADO E SE ESTA MAIOR QUE ZERO
 SE SIM ELE PERCORRE O ARRAY DE ERROS EXIBINDO AS MENSAGENS-->
@if(isset($errors) && count($errors) > 0)
<div class="alert alert-danger">
    @foreach($errors->all() as $error)
    {{$error}}<br>
    @endforeach
</div>
@endif

<!--
EXEMPLO DE COMO FICARIAM OS INPUTS

<form class="form" method="post" action="/produto">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        <input type="text" name="nome" placeholder="Insira o Nome" class="form-control">
    </div>
    <div class="form-group">
        <input type="email" name="email" placeholder="Insira o Email" class="form-control">
    </div>
    <div class="form-group">
        <input type="password" name="password" placeholder="Insira a Senha" class="form-control">
    </div>
    <div class="form-group">
        <input type="checkbox" name="check" value="Checkbox Value"> Marcar
    </div>
    <div class="form-group">
        <select name="estados" class="form-control">
            <option value=1>Opção 1</option>
            <option value=2>Opção 4</option>
            <option value=3>Opção 3</option>
            <option value=4>Opção 4</option>
        </select>
    </div>
    
    <div class="form-group">
        Sexo: <br>
        <input type="radio" name="sexo" value="M">Masculino
        <input type="radio" name="sexo" value="F">Feminino
    </div>
    
    <input type="submit" name="enviar" value="Enviar" class="btn btn-success">
</form>
-->


<form class="form" method="post" action="/produto">
    <!--<input type="hidden" name="_token" value="{{ csrf_token() }}">-->
    <!-- OU APENAS PASSAR ASSIM:-->
    {!! csrf_field() !!}

    <div class="form-group">
        <input type="text" name="nome" placeholder="Nome do Produto" class="form-control">
    </div>
    <div class="form-group">
        <input type="number" name="cod" placeholder="Código do Produto" class="form-control">
    </div>

    <input type="submit" name="enviar" value="Enviar" class="btn btn-success">
</form>



@endsection