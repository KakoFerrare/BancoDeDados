@extends('painel.templates.painel')
@section('content')

<br>
<div class="top-header">
    <h1 class="titulo">Deletar o Produto</h1>
    <br>
</div>


<form class="form" method="post" action="/produto/{{$produto->id}}">
    <!-- PRECISO CRIAR O INPUT ESPEFICANDO O TIPO DA REQUISIÇÃO -->
    <input name="_method" value="DELETE" type="hidden">
    {!! csrf_field() !!}

    <div>
        <b>Nome:</b> {{$produto->nome}}<br>
        <b>Código:</b>{{$produto->cod}}<br>
    </div>
    <a href="{{'/produto'}}" class="btn btn-primary"><i class="glyphicon glyphicon-backward"></i> Voltar</a>
    <input type="submit" name="enviar" value="Deletar" class="btn btn-danger">
</form>



@endsection