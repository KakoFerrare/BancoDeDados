<?php
//COMANDO PARA CRIAR MODEL: php artisan make:model Produto
//CRIAR NOME DA MODEL SEMPRE NO SINGULAR
namespace App\Models\Painel;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    //O LARAVEL JA ENTENDE PELO NOME DA MODEL A TABELA QUE VC ESTA ACESSANDO NO BANCO
    //ELE SEMPRE ATRIBUI UM S NO FINAL DO NOME DO MODELO
    
    //SE POR VENTURA UTILAR UM NOME DIFERENTE NA TABELA DA MODEL, A REPRESENTAÇÃO DICA ASSIM:
    //pŕotected $table = 'produtos';
    
    //SE EU NÃO DEFINIR MINHA CHAVE PRIMARIA COMO 'id' PARA 'id_produto', EU DEVO DEFINIR O ATRIBUTO AQUI
    //protected $primaryKey = 'id_produto';
    
    //SE EU NÃO QUISER QUE OS ATRIBUTOS CREATE_AT E UPDATE_AT SEJAM PREENCHIDOS AUTOMATICAMENTE EU DEVO USAR:
    //protected $timestamps = false;
    
    //SE EU FOSSE USAR OUTRA BASE DE DADOS,NÃO USANDO A DEFAULT CONFIGURADA NA BASE DO LARAVEL
    //EXEMPLO: ESSE MODELO VAI PASSAR POR OUTRO BANCO DE DADOS
    //BOM PARA USAR PARA UM WEBSERVICE
    //protected $connection = 'mysql2'; (EXEMPLO DE OUTRO BANCO CRIADO)
    
    
     //MODELO DE MASS ASSIGNMENT
    //USAR ESSE ATRIBUTO PARA SEGURANÇA DO SISTEMA, SEMPRE ESPECIFICAR QUAIS OS DADOS PODERAM SER
    //INSERIDOS OU EDITADOS
    //VAI SER USADO ATRAVEZ DO MÉTODO CREATE
    protected $fillable = ['nome','cod'];
    
    //SE CASO EU TIVER 50 ATRIBUTOS E FOR FICAR COMPLICADO DE ESPECIFICAR TUDO ISSO
    //EU PASSO OS DADOS QUE NÃO PODEM SER PREENCHIDOS
    //EXEMPLO
    //protected $guarded = ['create_at','update_at'];
    
    
    
    //CENTRALIZANDO AS REGRAS DO FORMULARIO
    static $rules =[
            'nome'=>'required|min:3|max:150',
        
        //AQUI DEFINI QUE O CÓDIGO SEMPRE VAI SER UNICO, QUE NÃO PODE REPETIR UM CADASTRO IDENTICO
        //E DEPOIS PASSO QUAL TABELA QUE ELA REPRESENTA
            'cod'=>'required|unique:produtos'
        ];
}
