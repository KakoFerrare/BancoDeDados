<?php
//LEMBRAR DE MUDAR O NAMESPACE
namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
//AQUI EU CRIEI UMA PASTA MODELS PARA MELHOR ORGANIZAR OS MODELOS, CRIEI MAIS DUAS PASTAS
//OS MODELS REFERENTE A PAINEL, VÃO FICAR NA PASTA PAINEL E ASSIM POR DIANTE, APENAS O USER QUE VAI FICAR 
//NA PASTA MODEL PARA PODER SER ACESSADO PELAS DUAS PASTAS
class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
