<?php

//UMA FORMA PARA RETORNAR USUÁRIOS
Route::get('users', function(){
   return DB::table('users')->get(); 
});
//UMA FORMA PARA RETORNAR USUÁRIOS


Route::get('/', function () {
    return view('welcome');
});




//QUERY BUILDER: SERVE PARA CRIAR CONSULTAS ESPECIFICAS NO LARAVEL
Route::get('query-builder', 'QueryBuilderController@testes');

Route::get('query-builder2', 'QueryBuilderController@testesDois');

Route::get('query-builder3', 'QueryBuilderController@testesTres');

Route::get('where', 'QueryBuilderController@where');

Route::get('query-builder4', 'QueryBuilderController@testesQuatro');

//QUERY BIULDER: SERVE PARA CRIAR CONSULTAS ESPECIFICAS NO LARAVEL




//ROTA PARA MODEL DE PRODUTO, APENAS PARA TESTE, NUNCA DEVE TER LÓGICA NAS ROTAS
Route::get('models', function(){
    $produtos = new \App\Models\Painel\Produto;
    
    return $produtos->get();
    
});
//ROTA PARA MODEL DE PRODUTO, APENAS PARA TESTE



//ROTA CRIADA APENAS PARA TESTE
Route::get('teste-models', 'TesteModelsController@index');
//ROTA CRIADA APENAS PARA TESTE

//CRIEI UMA ROTA PARA FAZER UM UPDADE, PASSEI UM PARAMETRO {id}
Route::get('teste-models-update/{id}', 'TesteModelsController@update');
//CRIEI UMA ROTA PARA FAZER UM UPDADE, PASSEI UM PARAMETRO {id}

//CRIEI UMA ROTA PARA FAZER UM DELETE, PASSEI UM PARAMETRO {id}
Route::get('teste-models-delete/{id}', 'TesteModelsController@delete');
//CRIEI UMA ROTA PARA FAZER UM UPDADE, PASSEI UM PARAMETRO {id}




//AULA PARA CRIAR O CRUD, CRIEI O CONTROLLER DE PRODUTO (ProdutoController) COM
//ARTISAN PASSANDO O ULTIMO PARAMETRO --resource, PARA CRIAR UM CONTROLLER RESTFULL
//QUE JA ME TRAZ OS MÉTODOS DE CRUD PRONTOS, UTILIZANDO A CLASSE REQUEST

//E A ROTA TAMBEM FOI CRIADA COM RESOURCE, ASSIM NÃO PRECISANDO APONTAR AS ROTAS COM OS MÉTODOS.
// LEMBRANDO QUE QUANDO EU MOVI O CONTROLLER PARA UMA ṔASTA EU TAMBEM TENHO QUE APONTAR A PASTA
//SEMPRE QUE FOR POSTAR UM FORM, EU PROCURO UTILIZAR O MIDDLEWARE WEB, POIS ELE TEM 
//TODAS AS CLASSES NECESSÁRIAS PARA VALIDAÇÃO E DEIXAR TUDO MAIS SEGURO
//TIVE QUE ALTERAR COMO DEFINE O MIDDLEWARE PARA FUNCIONAR
Route::group(['middleware'=> [],'web'], function(){
    Route::resource('produto', 'Painel\ProdutoController');
});