<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests;
use \App\Http\Controllers\Controller;
use \App\Models\Painel\Produto;
//QUANDO EU MOVI O CONTROLLER PARA A PASTA EU PERDI A HERANÇA DO CONTROLLER
//ENTÃO TENHO QUE PASSAR O CAMINHO CORRETO, UTILIZEI O USE E PASSEI O CAMINHO

class ProdutoController extends Controller
{
    //DI - DEPENDENCY INJECTION
    //PARA NÃO FICAR CHAMANDO TODA HORA A ESTANCIA ESTATICA DO PRODUTO
    //JA CRIEI UMA VARIAVEL PROTEGIDA QUE RECEBE UMA NOVA ESTANCIA
    //LEMBRA QUE SE MUDAR PRA DI EU TENHO QUE CHAMAR OS MÉTODOS REFERENTES DA CLASSE DESTA FORMA
    
    ////$produtos = Produto::paginate(4);    
    //PARA
    //$produtos = $this->produto->paginate(4);
    
    protected $produto, $request;
    public function __construct(Produto $produto, Request $request) {
       $this->produto = $produto;
       $this->request = $request;
    }
    public function index()
    {
        //RECUPERANDO TODOS OS PRODUTOS CADASTRADOS
        //$produtos = Produto::get();
        
        //AQUI IMPEMENTEI O MÉTODO DE PAGINAÇÃO E PASSEI QUANTOS REGISTROS QUERO POR PAGINA
        //DEPOIS QUE FIZ O DEPENDECY INJECTION EU MUDEI A FORMA DE CHAMAR

        //$produtos = Produto::paginate(4);
        $produtos = $this->produto->paginate(4);
        
        return view('painel/produtos/index', compact('produtos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('painel/produtos/create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //PEGO TODOS OS DADOS DO FORMULARIO COM O ALL
        //dd($request->all());

        //PEGO O INPUT ESPEFICICO DO FORMULARIO COM input('nome')
        //dd($request->input('nome'));

        //PEGO OS INPUTS QUE QUISER DO FORMULARIO COM only(['nome, 'email''])
        //dd($request->only(['nome', 'email']));
        
        //PEGO OS INPUTS EXCETO OS QUE PASSAR COM except(['nome, 'email''])
        //dd($request->except('nome'));
        
        //return 'método que faz o post para criar o produto';
        
        $dadosForm = $this->request->all();
        
        //DECLAREI UMA VARIAVEL QUE RECEBE DE UM MÉTODO A VALIDAÇÃO DO FORM
        //O MÉTODO ESPERA 2 A 3 PARAMENTROS
        // 1 - PASSO O REQUESTA ALL
        // 2 - OS CAMPOS E SUAS VALIDAÇÕES
        // 3 - É OPCINAL, É AS MENSAGENS QUE POSSO PERSONALIZAR
        
        //RETIREI AS REGRAS NO CONTROLLER E CENTRALIZEI NO 
        ////$validator = validator($dadosForm, [
        //    'nome'=>'required|min:3|max:150',
        //    'cod'=>'required'
        //]);
        
        //APÓS CENTRALIZAR AS VALIDAÇÕES EU CHAMO DESTA FORMA
        $validator = validator($dadosForm, Produto::$rules);
        
        //VERIFICO SE TEVE ERROS, TRUE OU FALSE
        if($validator->fails()){
            //SE FALSO ELE ENTRA NO REDIRECT PASSANDO OS ERROS COM O MÉTODO WITH ERRORS
            //E OS DADOS DO POST VOLTAM PARA OS INPUTS O O MÉTODO WITHINPUTS
            // NA VIU EU TRATEI COMO VOU EXIBIR OS ERROS
            return redirect('/produto/create')
            ->withErrors($validator)
                    ->withInput();
        }
        $insert = $this->produto->create($dadosForm);
        if($insert){
            return redirect('/produto');
        }
        //SE EU QUISER RESGATAR O ID DO PRODUTO QUE FOI INSERIDO
        //$insert->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produto = $this->produto->find($id);
        return view('painel.produtos.show', compact('produto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //RECUPERA O PRODUTO PELO SEU ID
        $produto = $this->produto->find($id);
        
        //RETORNA A VIEW PARA EDITAR OS DADOS
        return view('painel.produtos.create-edit', compact('produto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
         $dadosForm = $this->request->all();
        //CENTRALIZEI AS VALIADAÇÕES DO OBJETO NO MODEL E TROQUEI AS REGRAS
        //POIS ACRESCENTEI QUE COD É REGISTRO UNICO, MAS A EDIÇÃO ESTAVA VALIDANDO O MESMO
        //ENTÃO TIVE QUE ALTERAR AS REGRAS
        
        $rules =[
            'nome'=>'required|min:3|max:150',
            //AQUI EU SETO O ITEM QUE EU QUERO QUE ELE NÃO VERIFIQUE ,cod,{$id}
            'cod'=>"required|unique:produtos,cod,{$id}",
        ];
        //E CHAMO A MESMA VARIAVEL PARA VALIDAR
        $validator = validator($dadosForm, $rules);
        
        if($validator->fails()){
            return redirect("/produto/$id/edit")
            ->withErrors($validator)
                    ->withInput();
        }
        
        //RECUPERA O PRODUTO
        $produto = $this->produto->find($id);
        
        //EDITA O PRODUTO
        $update = $produto->update($this->request->all());
        if($update){
            return redirect('/produto');    
        }
        else{
        return redirect("produto/{$id}/edit")->withErrors('errors', 'Falha ao editar!');    
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produto = $this->produto->find($id);
        $delete = $produto->delete();
        if($delete){
        return redirect('/produto');    
        }else{
        return redirect("/produto/$id");        
        }
        
    }
}
