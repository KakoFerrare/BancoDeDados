<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;

class QueryBuilderController extends Controller {

    public function testes() {

        //SELECT
        //$users = DB::table('users')->get();
        //dd(DB::table('users')->get());
        
        //INSERT
        //FIZ UM INSERT DE UM ARRAY DIMENSIONAL
        //$INSERT RECEBE O MÉTODO DE INSERT, ELE RECEBE TRUE OU FALSE
        /*$insere = DB::table('users')->insert([
            [
                'name' => 'Marcos',
                'email' => 'marcosferrare@hotmail.com',
                'password' => bcrypt('reggae87')
            ],
            [
                'name' => 'Eu',
                'email' => 'eu@hotmail.com',
                'password' => bcrypt('reggae87')
            ]
        ]);
        dd($insere);
        */
        
        
        //UPDATE
        //$UPDATE RECEBE O MÉTODO DE UPDATE, ELE RECEBE TRUE OU FALSE
        //->where('email','!=','marcosferrare@hotmail.com')
        //COLUNA, CONDIÇÃO E VALOR
        //->where('email','marcosferrare@hotmail.com')
        ////COLUNA E VALOR
        
        /*$update= DB::table('users')->where('email','marcosferrare@hotmail.com')->update([
                'name' => 'Usuário Update',
                'email' => 'update@hotmail.com',
            
        ]);
        dd($update);
        */
        
        
        //DELETE
        //$DELETE RECEBE O MÉTODO DE DELETE, ELE RECEBE TRUE OU FALSE
        /*$delete = DB::table('users')->where('id',3)->delete();
        dd($delete);
        */
    }
    
    //QUERY BUILDER 2//
    public function testesDois(){
        
        //UM SELECT SIMPLES, NÃO RECOMENDADO, SEMPRE TENTAR USAR A PROPRIEDADES DO LARAVEL
        //return DB::select('select * from users');
        //MELHOR FORMA ABAIXO
        //return DB::table('users')->get();
        
        //FORMA DE RETORNAR POUCOS COLUNA EX(NOME, EMAIL) E AS
        //return DB::table('users')->select('name as nome','email')->get();
        //FORMA MAIS CORRETA PARA SE USAR UM SELECT ESPECIFICANDO AS COLUNAS
        //return DB::table('users')->pluck('name','email');
        
        //RETORNAR PRIMEIRO INDICE DA BUSCA, PRECISA SEMPRE COMBINAR COM WHERE
        //$firstUserMaiorQueDezoito = \App\User::where('name', 'Kako Ferrare')->first();
        //return $firstUserMaiorQueDezoito;
    }
    
    
     //QUERY BUILDER 3//
    public function testesTres(){
        
        //CONTADOR, CONTA REGISTROS DE UMA TABELA
        //return DB::table('users')->count();
        
        //MAIOR VALOR DE UM REGISTRO NA TABELA POR COLUNA(COD)
        //return DB::table('produtos')->max('cod');
        
        //MENOR VALOR DE UM REGISTRO NA TABELA POR COLUNA(COD)
        //return DB::table('produtos')->min('cod');
        
        //MÉDIA DE UM REGISTRO NA TABELA POR COLUNA(COD)
        //return DB::table('produtos')->avg('cod');
    }
    
    public function where(){
        
        //CLAUSULAS, VARIAS CLAUSULAS
        //$produtos = DB::table('produtos')->where('id','3')->get(); IGUAL
        
        //$produtos = DB::table('produtos')->where('nome','like','%Desodorante%')->get(); COM %LIKE%
        
        //$produtos = DB::table('produtos')->where('nome','like','%a%')->get(); COM %LIKE%
        
        //$produtos = DB::table('produtos')->where('id','<>','3')->get(); DIFERENTE
        
        //$produtos = DB::table('produtos')->where([['id','<>','3'],['id','<>','1']])->get(); 2 CONDIÇÕES
        
        //$produtos = DB::table('produtos')->where('id','<>','3')->orWhere('id','<>','2')->get(); WHERE E OR 
        
        //$produtos = DB::table('produtos')->whereIn('id',[1,2,3])->get(); IN
        
        //$produtos = DB::table('produtos')->whereNotIn('id',[1,3])->get(); NOT IN
        
        //$produtos = DB::table('produtos')->whereNull('created_at')->get(); COLUNA IGUAL A NULL
        
        //$produtos = DB::table('produtos')->select('id','nome')->whereNull('created_at')->get();
        
        //$produtos = DB::table('produtos')->select('id','nome')->whereNotNull('created_at')->get(); COLUNA NOT NULL
        
        //RANGE DE VALORES
        //$produtos = DB::table('produtos')->select('id','nome')->whereBetween('cod',[0, 10000000])->get(); BETWEEN(ENTRE)
        
        //$produtos = DB::table('produtos')->select('id','nome')->whereNotBetween('cod',[500, 100000])->get(); NOT BETWEEN(NÃO ESTA ENTRE)
        
        
        //return $produtos;
    
        
    }
    
    
    public function testesQuatro(){
        
        //UM SELECT DE 2 COLUNAS ORDENANDO POR NOME(ORDERBY)
        //$produtos = DB::table('produtos')->select('id','nome')->orderBy('nome')->get();
        
        ////UM SELECT DE 2 COLUNAS ORDENANDO POR NOME(ORDERBY ASC OU DESC)
        //$produtos = DB::table('produtos')->select('id','nome')->orderBy('nome', 'desc')->get();
        //$produtos = DB::table('produtos')->select('id','nome')->orderBy('nome', 'desc')->get();
        
        //AQUI EU SETO APARTIR DE QUAL REGISTRO EU QUERO QUE O SELECT PARTA SKIP(1)
        //MAS TENHO QUE DEFINIR ATÉ ONDE ELE TEM QUE IR, NO MAXIMO 4 RESULTADOS TAKE(4)
        //COMO UMA PAGINAÇÃO
        $produtos = DB::table('produtos')->select('id','nome')->skip(1)->take(4)->get();
        return $produtos;
        
    }

}
