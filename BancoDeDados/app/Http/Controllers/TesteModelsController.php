<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use \App\Models\Painel\Produto;

class TesteModelsController extends Controller
{
    public function index(){
        
        $produto = new Produto;
        
        //OUTRA FORMA DE SALVAR NO BANCO, AINDA NÃO É TÃO RECOMENDADO
        //ELE FAZ O INSERT NOS ATRIBUTOS CREATE_AT E UPDATE_AT
        /*
        $produto->nome = 'Um Outro Produto';    
        $produto->cod = 232324;
        $produto->save();
        */
        
       
        //USAR O CREATE SEMPRE PARA QUANDO FOR FAZER A INSERÇÃO
        //LEMBRAR DE PASSAR O CODIGO COMO STRING
        return $produto->create(['nome'=>'Produto de Higiene','cod'=>'3345']);
        
    }
    
    public function update($id){
        
        $produto = Produto::find($id);

        //AQUI COM O SAVE NÃO PRECISO PASSAR O UPDATE, PQ DEI O FIND E DEPOIS PREENCHI OS DADOS,
        //ENTÃO ELE JA INTERPRETA UM MÉTODO DE UPDATE
        /*$produto->nome = 'Um Outro Produto';    
        $produto->cod = 232324;
        $produto->save();
         */
        
        $produto->update(['nome'=>'Produtão','cod'=>'333']);
    }
    
    public function delete($id){
        
        //$produto = Produto::find($id);
        //dd($produto->delete());
        
        //OU MAIS FACIL
        Produto::find($id)->delete();
    }
    
}
