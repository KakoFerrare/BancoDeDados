<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    //PARA RODAR O COMANDO SEEDER EU USO: php artisan db:seed
    //PARA RODA APENAS UMA SEEDER ESPECIFICA: php artisan db:seed --class=ProdutosTableSeeder
    public function run() {
        // $this->call(UsersTableSeeder::class);
        
        //APÓS CRIAR MINHAS CLASSES DE SEEDER EU CHAMO ELAS DESTA FORMA
        $this->call(UsersTableSeeder::class);
        $this->call(ProdutosTableSeeder::class);
        
        
    //AQUI EU PRECISO CRIAR UM USUARIO PARA LOGAR E CADASTRAR NOVOS USUÁRIOS, UM USUÁRIO INICIAL
    //COM A CLASSE DE SEEDS EU CONSIGO CRIAR ESTE USUÁRIO PELO UM COMANDO DO ARTISAN EXECUTANDO
    //O CÓDIGO ABAIXO
    //DB::table('users')->insert([
    //    'name'=>'Kako Ferrare',
    //    'email'=> 'kakoferrare87@gmail.com',
    //    'password'=>  bcrypt('reggae87'),
        //ESTOU CRIPTOGRAFANDO E SENHA COM A FUNÇÃO BCRYPT
    //]);
        
    //PASSEI TODO O COMANDO PARA A SEEDER DO PRÓPIO OBJETO
        
    }

}
