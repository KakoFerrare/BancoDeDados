<?php

use Illuminate\Database\Seeder;

class ProdutosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('produtos')->insert([
        'nome'=>'Sabonete',
        'cod'=> 99999,
    ]);
    }
}
