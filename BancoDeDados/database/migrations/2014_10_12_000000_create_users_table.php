<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //ONDE É CRIADA A ESTRUTURA DA TABELA
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            //INCREMENTES DEFINE PRIMARY KEY E AUTOINCREMENTO
            $table->increments('id');
            //DEFINE UMA STRING
            $table->string('name');
            //DEFINE UMA STRING MAS UE VAI SER UNICO
            $table->string('email')->unique();
            //DEFINE UMA STRING COM O MAXIMO DE 60 CARACTERES
            $table->string('password',60);
            //QUANDO USUARIO SOLICITAR UMA NOVA SENHA ESSE CAMPO SERÁ PREENCHIDO
            //CAMPO APENAS PARA USUÁRIOS
            $table->rememberToken();
            //DEFINE 2 COLUNAS CREATE AT E UPDATE AT, PARA GUARDAR CRIAÇÕES E UPDATE NA TABELA
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    //ONDE DELETA A TABELA, O ARTISAN CRIA E DELETA TABELAS, COM O COMANDO REFRESH
    public function down()
    {
        Schema::drop('users');
    }
}

//COMANDO DO ARTISAN MIGRATE PARA GENRENCIAR ESSES A BASE:
//:install (ELE RODA A MIGRATIONS E INSTALA A BASE.)
//:refresh (ELE O COMANDO DROP QUE APAGA A SABE E DEPOIS INSTALA NOVAMENTE, É BOM FAZER SE CASO
//ALTERAR ALGUMA TABELA.)
//:reset (DA UM ROLLBACK EM TODAS MIGRATIONS)
//:rollback (DA UM ROLLBACK NA ULTIMA DATABASE MIGRATION)
//:status (EXIBE O STATUS DE TODAS A MIGRATIONS)