<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

//SEMPRE ME ATENTAR AO NOME QUE CRIO COM O COMANDO DE MIGRATIONS DO ARTISAN:
//php artisan make:migration create_produtos_table
//SEMPRE COM O NOME create_(NOME DO OBJETO EM PLURAL)_table, USAR SEMPRE ESSE PADRÃO POIS SERÁ
// O MESMO NOME QUE VAI SER DADO PARA A TABELA DO BANCO (produtos).
//SE POR VENTURA EU QUERER DEIXAR NO SINGULAR E APONTAR UMA TABELA QUE SERA CRIADA COM O NOME
//EM PLURAL, USO O COMANDO:
//php artisan make:migration create_produtos_table --table=produtos

//RODEI UM MIGRATE:REFRESH PARA ATUALIZAR A TABELA, ELE APAGA TODOS OS REGISTROS

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Uso a classe Schema e digo que vou criar uma base
        Schema::create('produtos', function(Blueprint $table){
            $table->increments('id');
            $table->string('nome',100);
            $table->integer('cod');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //AQUI CHAMO A FUNÇÃO DE DROP TABLE, CHAMO O SCHEMA COM A FUNÇÃO DROP E O NOME DA TABELA
        Schema::drop('produtos');
    }
}
